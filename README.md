CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The CKEditor Print Button module integrates the CKEditor "Print" plugin.

This plugin activates the printing function. A standard operating system
printing pop-up window will appear where you will be able to choose the printer
as well as all relevant options.

 * For a full description of the module visit:
   https://www.drupal.org/project/ckeditor_print

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/ckeditor_print


REQUIREMENTS
------------

This module requires the following outside of Drupal core.

 * https://ckeditor.com/addon/print


INSTALLATION
------------

 * Install the CKEditor Print Button module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

How To Install Dependencies Via Composer:
    1. Add ckeditor/tableselection repositories to your `composer.json`.

```
"repositories": [
    {
        "type": "package",
        "package": {
            "name": "ckeditor/print",
            "version": "4.9.1",
            "type": "drupal-library",
            "dist": {
                "url": "https://download.ckeditor.com/print/releases/print_4.9.1.zip",
                "type": "zip"
            }
        }
    }
],
```
    2. Execute `composer require ckeditor/print`.
    3. Make sure there is the file `libraries/print/plugin.js`.

How To Install Dependencies Manually:
    1. Download the plugin from the project page:
       https://ckeditor.com/addon/print
    2. Create a libraries folder in the drupal root if it doesn't exist.
    3. Extract the plugin archive in the librairies folder.
    4. Make sure there is the file `libraries/print/plugin.js`.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Content > Formats and select
       the format to edit.
    3. Add the Print plugin button in the editor toolbar.
    4. Save, that's it, "Print" plugin will automatically start to work once the
       "Print" button is in the CKEditor toolbar.


MAINTAINERS
-----------

 * Oleksandr Chernyi (BlacKICEUA) - https://www.drupal.org/u/blackiceua
